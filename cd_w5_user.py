import hashlib
import string
import random
 
''' sha1 secure hashes '''
 
# convert user_password into sha1 encoded string
def gen_password(user_password):
    return hashlib.sha1(user_password.encode("utf-8")).hexdigest()
 
# generate random user password
def user_password(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def gen_users_auth_php(stud_list_filename, outputdir, sep):
    # read lines from file
    #lines = open("2b_stud_list.txt", "r", encoding="utf-8").read().splitlines()
    #lines = open(stud_list_filename, "r", encoding="utf-8").read().splitlines()
    # we may also need to notice every user with computer generated passwords
    file_header = '''
# users.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Userfile
#
# Format:
#
# login:passwordhash:Real Name:email:groups,comma,seperated
# smd5 admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user

admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user
'''
    text = 'abc%03d'
    outputfile = open(outputdir+"/users.auth.php", "w", encoding="utf-8")
    outputfile.write(file_header)
    for i in range(1,399+1):
        password = text%(i)
        #user_account = lines[i].split("\t")[0]+"_gm.nfu.edu.tw"
        user_account = text%(i)
        computer_generated_password = password
        sha1_password = gen_password(password)
        real_name = text%(i)
        email =text%(i)+"@gm.nfu.edu.tw"
        groups = "user"
        '''
        print(lines[i].split(",")[0], "-", password, "-", gen_password(password), \
                "-", lines[i].split(",")[1])
        '''
        #print(text%(i),gen_password(text%(i)))
       
        print(user_account+":"+password)
        line_to_write = user_account+sep+sha1_password+sep+real_name+sep+email+sep+groups+"\n"
        outputfile.write(line_to_write)
    outputfile.close()
        

# call gen_users_auth_php() to generate users_auth.php file
gen_users_auth_php("../w8/_users.txt", "./", sep=":")
print("done")